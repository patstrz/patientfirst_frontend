import axios from 'axios';
import { browserHistory } from 'react-router';
import { AUTH_USER, AUTH_ERROR, UNAUTH_USER, SIGNUP_ERROR } from './types';
import {AUTH_TOKEN, ROOT_URL} from './api_commons';

export function signinUser({username, password}) {
  return function(dispatch){
  // es6 syntax shortcut bellow because key and value are identical.
  axios.post(`${ROOT_URL}/api/auth/token/`,{username, password})
  //if authenticated
    .then(response => {
      //dispatches action
      dispatch({type:AUTH_USER});

      localStorage.setItem('token', response.data.token);
      //redirects
      browserHistory.push('/feature');
      })
    .catch( () => {
      dispatch(authError('Bad Login Info', AUTH_ERROR))
    });
  }
}

export function signupUser({username,email, password}) {
  // return function(dispatch){
  // es6 syntax shortcut bellow because key and value are identical.
  return function (dispatch){
  axios.post(`${ROOT_URL}/api/register`,{username, email,password})
  //if authenticated
    .then(response => {
      dispatch({type: AUTH_USER});
      localStorage.setItem('token', response.data.token);
      browserHistory.push('/feature');
      //empties the SIGNUP_ERROR case reducers by not passing in an empty
      //payload,so won't have left over errors remaining on the signup form
      //after signing up succesfully.
      dispatch({
        type:SIGNUP_ERROR,
        payload:''
              });

    })
    .catch( response =>{
      dispatch(authError(response.data, SIGNUP_ERROR));

    }
  );

  }
  }

export function authError(error, error_type) {
  return {
    type: error_type,
    payload: error
  }
}

export function signoutUser() {
  localStorage.removeItem('token');
  return {type: UNAUTH_USER};
}
