export const AUTH_USER = 'auth_user';
export const  UNAUTH_USER = 'unauth_user';
export const  AUTH_ERROR = 'auth_error';
export const  SIGNUP_ERROR = 'signup_error';
export const FETCH_HOME = 'fetch_posts_list';
export const FETCH_POST = 'fetch_post';
export const CREATE_POST = 'create_post';
export const DELETE_POST = 'delete_post';
