import axios from 'axios';
import { browserHistory } from 'react-router';
import { AUTH_USER } from './types';
import { AUTH_ERROR } from './types';
import { UNAUTH_USER } from './types';
import { SIGNUP_ERROR } from './types';
import { FETCH_HOME } from '../actions/types';
import { CREATE_POST } from '../actions/types'
const AUTH_TOKEN = `JWT ${localStorage.getItem('token')}`
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

const ROOT_URL = 'http://localhost:8000';
// const ROOT_URL = 'https://patientfirstrest2.herokuapp.com'

export function signinUser({username, password}) {
  return function(dispatch){
  // es6 syntax shortcut bellow because key and value are identical.
  axios.post(`${ROOT_URL}/api/auth/token/`,{username, password})
  //if authenticated
    .then(response => {
      //dispatches action
      dispatch({type:AUTH_USER});

      localStorage.setItem('token', response.data.token);
      //redirects
      browserHistory.push('/feature');
    })
    .catch( () => {
      dispatch(authError('Bad Login Info', AUTH_ERROR))
    });
  }
}

export function signupUser({username,email, password}) {
  // return function(dispatch){
  // es6 syntax shortcut bellow because key and value are identical.
  return function (dispatch){
  axios.post(`${ROOT_URL}/api/register`,{username, email,password})
  //if authenticated
    .then(response => {
      dispatch({type: AUTH_USER});
      localStorage.setItem('token', response.data.token);
      browserHistory.push('/feature');
      //empties the SIGNUP_ERROR case reducers by not passing in an empty
      //payload,so won't have left over errors remaining on the signup form
      //after signing up succesfully.
      dispatch({
        type:SIGNUP_ERROR,
        payload:''
              });

    })
    .catch( response =>{
      dispatch(authError(response.data, SIGNUP_ERROR));

    }
  );

  }
  }

export function authError(error, error_type) {
  return {
    type: error_type,
    payload: error
  }
}

export function signoutUser() {
  localStorage.removeItem('token');
  return {type: UNAUTH_USER};
}

export function fetchHome() {
  return function(dispatch){
    axios.get(`${ROOT_URL}/api/posts/`, {
      headers: {authorization: `JWT ${localStorage.getItem('token')}`}
    })
    .then(response => {
      dispatch({
        type: FETCH_HOME,
        payload: response.data
      });
    });
  }
}

export function createPost(props) {
  const request = axios.post(`${ROOT_URL}/api/newpost/`,{'title':props.title,
    'description':props.description}
  );
return{
  type: CREATE_POST,
  payload: request
};

}

// export function createPost({title,description}) {
//   // return function(dispatch){
//   // es6 syntax shortcut bellow because key and value are identical.
//   return function (dispatch){
//   axios.post(`${ROOT_URL}/api/newpost`,{
//     headers: {"Authorization": `JWT ${localStorage.getItem('token')}`},
//   })
//   //if authenticated
//   }
// }
