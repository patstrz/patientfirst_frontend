import axios from 'axios';
import { browserHistory } from 'react-router';
import { FETCH_HOME,FETCH_POST,CREATE_POST,DELETE_POST} from '../actions/types';
import {AUTH_TOKEN, ROOT_URL} from './api_commons';


function replacer(key, value) {
  if (value instanceof FileList) {
    return Array.from(value).map(file => file.name).join(', ') || 'No Files Selected';
  }
  return value;
}

function stringify(values) {
  return JSON.stringify(values, replacer, 2);
}



export function fetchPostsList() {
  return function(dispatch){
    axios.get(`${ROOT_URL}/api/posts/`, {
      headers: {authorization: `JWT ${localStorage.getItem('token')}`}
    })
    .then(response => {
      dispatch({
        type: FETCH_HOME,
        payload: response.data
      });
    });
  }
}

export function createPost(props) {
  const request = axios.post(`${ROOT_URL}/api/newpost/`,props);
    // console.log('props image:', props.image.item(0))
  return{
    type: CREATE_POST,
    payload: request
  };
}

// export function fetchPost(slug) {
//   const request = axios.get( `${ROOT_URL}/api/posts/${slug}`);
//   console.log("request:",request)
//   return {
//     type: FETCH_POST,
//     payload: request
//   }
// }

export function fetchPost(slug) {
  return function(dispatch){
    axios.get(`${ROOT_URL}/api/posts/${slug}`, {
      headers: {authorization: `JWT ${localStorage.getItem('token')}`}
    })
    .then(response => {
      dispatch({
        type: FETCH_POST,
        payload: response.data
      });
    });
  }
}

export function deletePost(slug) {
  return function(dispatch){
    axios.delete(`${ROOT_URL}/api/posts/${slug}`, {
      headers: {authorization: `JWT ${localStorage.getItem('token')}`}
    })
    .then(()=> {
      console.log('it just fuggin happend no?');
      browserHistory.push('/feature');
      // dispatch({type: DELETE_POST });
      // browserHistory.push('/feature');
    })
    .catch( () => {
      dispatch(authError('Bad Login Info', AUTH_ERROR))
    });
  }
}


export function signinUser({username, password}) {
  return function(dispatch){
  // es6 syntax shortcut bellow because key and value are identical.
  axios.post(`${ROOT_URL}/api/auth/token/`,{username, password})
  //if authenticated
    .then(response => {
      //dispatches action
      dispatch({type:AUTH_USER});

      localStorage.setItem('token', response.data.token);
      //redirects
      browserHistory.push('/feature');
      })
    .catch( () => {
      dispatch(authError('Bad Login Info', AUTH_ERROR))
    });
  }
}
