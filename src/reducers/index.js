import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from './auth_reducer';
import postsReducer from './posts_reducer';
import { reducer as formReducer } from 'redux-form';
const rootReducer = combineReducers({
  //es6 for form: form
  form:formReducer,
  auth: authReducer,
  posts: postsReducer
});

export default rootReducer;
