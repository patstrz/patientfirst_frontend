import { AUTH_USER } from '../actions/types';
import { UNAUTH_USER } from '../actions/types';
import { AUTH_ERROR } from '../actions/types';
import { SIGNUP_ERROR } from '../actions/types';
import { FETCH_HOME } from '../actions/types';

function change_username_error(payload){
  if (payload  == 'MyUser with this username already exists.'){
    return 'Username already exists, please choose another username :)'
  }
  else {
    return payload
  }
}

const INITIAL_STATE = { home:[]};

export default function(state= INITIAL_STATE, action) {

  switch(action.type){
    case AUTH_USER:
      return { ...state, authenticated: true };
    case UNAUTH_USER:
      return { ...state, authenticated: false};
    case AUTH_ERROR:
      return { ...state, errors: action.payload};
    case SIGNUP_ERROR:
      return { ...state,
      signup_error_username: change_username_error(action.payload.username),
      signup_error_email: action.payload.email,
      signup_error_password: action.payload.password};
    case FETCH_HOME:
      return { ...state, home: action.payload};


  }
  return state;
}
