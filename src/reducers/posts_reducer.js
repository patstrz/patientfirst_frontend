import { FETCH_HOME, FETCH_POST, DELETE_POST } from '../actions/types';
const INITIAL_STATE = { home:null, post:[]};

export default function(state= INITIAL_STATE, action) {

  switch(action.type){

    case FETCH_HOME:
      return { ...state, posts_list: action.payload};

    case FETCH_POST:
      return { ...state, post: action.payload};

    case DELETE_POST:
        return { ...state, post: null};
  }
  return state;
}
