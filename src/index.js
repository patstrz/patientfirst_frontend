import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory} from 'react-router';
import reduxThunk from 'redux-thunk';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Feature from './containers/feature';
import PostsNew from './containers/posts/posts_new';
import PostsDetailContainer from './containers/posts/posts_detail_container';
import RequireAuth from './components/auth/require_auth';
import Welcome from './components/welcome';
import reducers from './reducers';
import {AUTH_USER} from './actions/types';

// var FontAwesome = require('react-fontawesome');


const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
//To make sure we authenticate before react tries to render components.\
const token = localStorage.getItem('token');

if (token) {
  store.dispatch({type:AUTH_USER})
}

ReactDOM.render(
  <Provider store={store}>
    <Router history= {browserHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={Feature}/>
        <Route path="signin" component={Signin}/>
        <Route path="signout" component={Signout}/>
        <Route path="signup" component={Signup}/>
        <Route path="feature" component={RequireAuth(Feature)}/>
        <Route path="newpost" component={RequireAuth(PostsNew)}/>
        <Route path="posts/:slug" component={PostsDetailContainer}/>
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.container'));
