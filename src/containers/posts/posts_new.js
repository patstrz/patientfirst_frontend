import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { createPost } from '../..//actions/posts_actions';
import { Link } from 'react-router';
//https://www.npmjs.com/package/redux-file-upload --

function replacer(key, value) {
  if (value instanceof FileList) {
    return Array.from(value).map(file => file.name).join(', ') || 'No Files Selected';
  }
  return value;
}

function stringify(values) {
  return JSON.stringify(values, replacer, 2);
}


class PostsNew extends Component {
  //  Gives you access to this.context.router inside of a component
  // don't dwell too much on why it is available, just know that you
  //can access router in grandchild component without passing a callback through
  //the parent.


  static contextTypes = {
    router: PropTypes.object
  };

  onSubmit(props) {
    // props.image = stringify(props.image);
    //props passed in because for fields are in the props. need this.props
    //because the action creator createPost is in props.
    this.props.createPost(props)
    // .then(()=> {
        this.context.router.push('/');
      // });
}


  render() {
    const { fields: {title, description, content} , handleSubmit} = this.props;
    // above is es6 equivalent of es5's: const title = this.props.fields.title; ...
    return (
      //if form is valid handleSubmit calls the action creator handleSubmit
      // <form onSubmit= {handleSubmit(this.props.createPost)}>
      //code above was from before the helper functiion onSubmit was created
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <h3>Create a New Post</h3>
        <div className={`form-group ${title.touched && title.invalid ? 'has-danger' :''}`}>
          <label>Title</label>
          {/*...title is the the passing of the configuration
            object to the input allowing form element to be controlled by
            redux form*/}
          <input type="text" className="form-control" {...title}/>
            <div className="text-help text-primary">
              {/*javascript ternary expression. if title is touched return title
              error if false return empty string. */}
              {title.touched ? title.error : ''}
          </div>
        </div>

        <div className={`form-group ${description.touched && description.invalid ? 'has-danger' :''}`} >
          <label>Description</label>
          <input type="text" className="form-control" {...description}/>
            <div className="text-help text-primary">
              {description.touched ? description.error : ''}
          </div>
        </div>

        {/*<div>
           <input type="file" className="form-control" {...image} value={ null }/>
         </div>*/}
         <div className={`form-group ${content.touched && content.invalid ? 'has-danger' :''}`}>
           <label>Content</label>
           <textarea className="form-control" {...content}/>
             <div className="text-help text-primary">
               {content.touched ? content.error : ''}
           </div>
         </div>

        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/" className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

function validate(values){
  const errors = {};

  if (!values.title){
    errors.title = 'Enter a title';
  }
  if (!values.description){
    errors.description = 'Enter a description';
  }

  // if (!values.image){
  //   errors.image = 'Please upload an image';
  // }
  if (!values.content){
    errors.content = 'Please provide content';
  }
  return errors;
}

//here you pass in configuration to redux-form , tell redux-form what
// name of the form is and what fields it is in charge of.
//connect first argument is mapStateToProps, 2nd is mapDispatchToProps
//redux form first is form config, 2nd is mapStateToProps 3rd is
//mapDispatchToProps
export default reduxForm({
  form: 'PostsNewForm',
  fields: ['title', 'description', 'content'],
  validate
}, null, {createPost})(PostsNew);

// whenever user types something in it is recorded in the Global
// application state, instead of component level state. pseudocode:
// state === {
//   form: {
//     PostsNewForm: {
//       title: '....',
//       categories: '....',
//       content: '....'
//     }
//   }
// }
