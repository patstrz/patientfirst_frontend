import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/posts_actions';
import Subpost from '../../components/subpost';
import Loading from '../../components/utilities/loading';
import { Link } from 'react-router';




var FontAwesome = require('react-fontawesome');

class PostsList extends Component {

  getImageUrl(url){
    if(url){
      var urls = url.split('?');
      return urls[0]
    }
  }

  getSubPosts(post){
    return post.subposts.map((subpost)=>{
      return(
          <Subpost key={subpost.title} subpost={subpost} />
      );
    });
  }

  getPosts(){

    if (!this.props.authenticated || this.props.authenticated === false ){
      return (<div><h2>Please sign in to view posts !</h2></div>);
    }

    if (!this.props.home){
      return (
        <Loading/>
      );
    }

    return this.props.home.map((post,index)=> {
      return(
        <div className = "col-sm-12" key={post.id+post.title}>
        <li className="list-group-item" >
          <div className= "col-sm-2">
          <img className= "thumb"src={this.getImageUrl(post.image)} />
          </div>
          <h4 className="col-sm-6">{post.title}</h4>
          <p>{post.description}</p>
          <Link to={"/posts/"+post.slug}>View Post</Link>
        </li>
          <br></br>
          <div>
            {this.getSubPosts(post)}
          </div>

        </div>
      );
    });
  }

  componentWillMount(){
    this.props.fetchPostsList();
  }
  render(){
    return(
      <div>
        <ul>
          {this.getPosts()}
        </ul>
        </div>
    );
  }
}

function mapStateToProps(state){
  return { home: state.posts.posts_list ,
    authenticated: state.auth.authenticated
  }
}
export default connect(mapStateToProps, actions)(PostsList);
