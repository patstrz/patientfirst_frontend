import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchPost, deletePost } from '../../actions/posts_actions';
// import { Link } from 'react-router';


class PostsDetailContainer extends Component {
  //to give access to router in context
  static contextTypes = {
    router: PropTypes.object
  };
  componentWillMount(){
    this.props.fetchPost(this.props.params.slug);

  }

  getImageUrl(url){
    if(url){
      var urls = url.split('?');
      return urls[0]
    }
  }


  onDeleteClick(){
    this.props.deletePost(this.props.params.slug)
      // .then(()=> {this.context.router.push('/');})
  }
  render(){


    // const post = this.props.post
    //es6 syntax for line above
    const {post} = this.props;

    // if (!this.props.post){
    //   return <div>Loading...</div>;
    // }
    return(
      <div>
        {/*<Link to="/">Back To Index</Link>*/}
        <button className="btn btn-danger pull-xs-right"
           onClick={this.onDeleteClick.bind(this)}>
          Delete Post
        </button>
        {/*<h3>{post.title}</h3>*/}
        <h2>Post Title: {post.title}</h2>
        <img className= "thumb"src={this.getImageUrl(post.image)} />



      </div>

    );
    // console.log(this.props.post)
    // return <div>show post:{this.props.params.slug}</div>

  }
}
function mapStateToProps(state){
  return { post: state.posts.post }
}
export default connect(mapStateToProps, {fetchPost, deletePost})(PostsDetailContainer);
// export default connect('',{fetchPost, deletePost})(PostsShow);
