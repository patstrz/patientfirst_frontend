import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class Header extends Component {
  renderLinks() {
    if (this.props.authenticated){
      return(
        <div>
        <li className="nav-item">
          <Link className="nav-link" to="/newpost">Create Post</Link>
        </li>
      <li className="nav-item">
        <Link className="nav-link" to="/signout">Sign Out</Link>
      </li>
    </div>
    );
    }
    else {
      //square braces = react syntax for rendering array of html components
      return[
      <li className="nav-item">
        <Link className="nav-link" to="/signin">Sign In</Link>
      </li>,
      <li className="nav-item">
        <Link className="nav-link" to="/signup">Sign up</Link>
      </li>
    ];
    }

  }
  render() {
    return (
      <div>
      <nav className= "navbar navar-light">
        <Link to = "/" className="navbar-brand">Patientfirst</Link>
        <ul className="nav navbar-nav">
          {this.renderLinks()}
        </ul>

      </nav>
      <br></br>
      <br></br>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}
export default connect(mapStateToProps)(Header);
