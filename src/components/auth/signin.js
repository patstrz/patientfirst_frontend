import React, {Component} from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions/auth'

class Signin extends Component {
  handleFormSubmit({username, password}){
    // sign in user is an action
    this.props.signinUser({ username, password});
  }

  renderAlert(){
    if (this.props.errorMessage){
      return (
        <div className="alert alert-danger">
          <strong>Ooops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }
  render(){
    //
    const { handleSubmit, fields: {username, password}} = this.props;
    return(
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
      <fieldset className="form-group">
        <label>Username:</label>
        <input{...username} className="form-control"/>
      </fieldset>
      <fieldset className="form-group">
        <label>Password:</label>
        <input type="password" {...password} className="form-control"/>
      </fieldset>
      {this.renderAlert()}
      <button action="submit" className="btn btn-primary">Sign in</button>
    </form>
    );
  }
}

//helper function for redux form
//Make sure form reducer is imported in reducers/index

//redux form helper works just like connect helper
// first argument is form info , second is mapStateToProps, third
// actions
function mapStateToProps(state){
  return { errorMessage: state.auth.errors }
}

export default reduxForm({
  form: 'signin',
  fields: ['username','password']
}, mapStateToProps, actions)(Signin);
