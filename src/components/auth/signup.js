import React, {Component} from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions/auth'

class Signup extends Component {

  handleFormSubmit(formProps) {
    this.props.signupUser(formProps);
  }


  renderAlert(error){
    //don't know why but breaks if only put this.props.errorMessage[field_name]
    if (this.props[error]){

      return (
        <div >
          <br/>
          <div className = " alert alert-danger">
            <strong>Oops!</strong> {this.props[error]}
          </div>
        </div>
      );

    }
  }

render(){
    const { handleSubmit, fields: {username, email, password, passwordConfirm}} = this.props;
    return(
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
      <fieldset className="form-group">
        <label>Username:</label>
        <input{...username} className="form-control"/>
        {/*js syntax shortform : if __ and __ return <div></div>*/}
        {username.touched && username.error && <div className="error">{username.error}</div>}
        {this.renderAlert('errorUsername')}
      </fieldset>
      <fieldset className="form-group">
        <label>Email:</label>
        <input{...email} className="form-control"/>
        {/*js syntax shortform : if __ and __ return <div></div>*/}
        {email.touched && email.error && <div className="error">{email.error}</div>}
        {this.renderAlert('errorEmail')}
      </fieldset>
      <fieldset className="form-group">
        <label>Password:</label>
        <input type="password" {...password} className="form-control"/>
        {/*client side auth*/}
        {password.touched && password.error && <div className="error">{password.error}</div>}
        {/*//server side auth*/}
        {this.renderAlert('errorPassword')}
      </fieldset>
      <fieldset className="form-group">
        <label>Confirm Password:</label>
        <input type="password" {...passwordConfirm} className="form-control"/>
        {passwordConfirm.touched && passwordConfirm.error && <div className="error">{passwordConfirm.error}</div>}
      </fieldset>
      <button action="submit" className="btn btn-primary">Sign up!</button>
    </form>
    );
};
}

// function mapStateToProps(state){
//   return { errorMessage: state.auth.error }
// }

function validate(formProps) {
  const errors = {};
  if (!formProps.username){
    errors.username = 'Please enter an username'
  }
  if (!formProps.email){
    errors.email = 'Please enter an email'
  }

  if (!formProps.password){
    errors.password = 'Please enter a password';
  }

  if (!formProps.passwordConfirm){
    errors.passwordConfirm = 'Please enter a password';
  }

  if (formProps.password !== formProps.passwordConfirm){
    errors.password = 'Passwords must match';
  }
  
  return errors;
};



function mapStateToProps(state) {
  //server side error say MyUser and not able to change that.

  return { errorUsername: state.auth.signup_error_username,
    errorEmail: state.auth.signup_error_email,
    errorPassword: state.auth.signup_error_password,
   };
}

export default reduxForm({
  form: 'signup',
  fields: ['username','email','password','passwordConfirm'],
  validate
}, mapStateToProps, actions)(Signup);
